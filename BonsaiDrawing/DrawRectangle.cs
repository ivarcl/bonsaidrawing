﻿using System;
using System.Linq;
using System.Text;
using OpenCV.Net;
using System.ComponentModel;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Bonsai;

namespace Bonsai.Drawing
{
    [Description("Draws a rectangle into the image")]
    public class DrawRectangle : Transform<IplImage, IplImage>
    {
        [Description("X coordinate of rectangle")]
        public int Left { get; set; }

        [Description("Y coordinate of rectangle")]
        public int Top { get; set; }

        [Description("Width of the rectange in pixels")]
        public int Width { get; set; }

        [Description("Height of the rectangle in pixels")]
        public int Height { get; set; }

        [Description("Color of the new rectangle")]
        public Scalar Color { get; set; }


        public DrawRectangle()
        {
            Left = 50;
            Top = 50;
            Width = 1920 - 101;
            Height = 1080 - 101;

            Color = new Scalar(255, 255, 255);
        }

        public override IObservable<IplImage> Process(IObservable<IplImage> source)
        {
            return source.Select(input =>
            {
                var output = new IplImage(input.Size, input.Depth, input.Channels);
                CV.Copy(input, output);

                Rect rect = new Rect(Left, Top, Width, Height);

                CV.Rectangle(output, rect, Color, -1);

                return output;
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using OpenCV.Net;
using Bonsai;

namespace Bonsai.Drawing
{
    [Description("Creates an empty image")]
    class Image : Source<IplImage>
    {
        [Description("Width of the image in pixels")]
        public int Width { get; set; }

        [Description("Height of the image in pixels")]
        public int Height { get; set; }


        public Image()
        {
            Width = 1920;
            Height = 1080;
        }


        public override System.IObservable<IplImage> Generate()
        {
            return Observable.Defer(() =>
            {
                Size size = new Size(Width, Height);

                var image = new IplImage(size, IplDepth.U8, 3);
                image.SetZero();

                return Observable.Return(image);
            });
        }
    }
}
